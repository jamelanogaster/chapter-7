const express = require('express');
const Joi = require('joi');

const app = express();

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '../../'));
app.use(express.static(__dirname + '../../DEAChap4'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const users = [
    { id: 1, email: "test@gmail.com", password: "testing" },
];

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/game', (req, res) => {
    res.render('game');
});

app.get('/api/users', (req, res) => {
    res.json(users);
});

app.post('/api/users', (req, res) => {
    const existingUser = users.find((user) => {
        return user.email == req.body.email
    });

    if (existingUser == null) {
        const result = validateUser(req.body);
        const { error } = validateUser(req.body);

        if (error) return res.status(400).send(error.details[0].message);

        const user = { id: users.length + 1, email: req.body.email, password: req.body.password };
        users.push(user);
        res.json(result);
    } else return res.status(400).json({
        status: 'failed',
        errors: 'email has been used'
    });
});

app.post('/api/login', (req, res) => {
    const { error } = validateUser(req.body);

    if (error) return res.status(400).send(error.details[0].message);

    const email = req.body.email;
    const password = req.body.password;

    const existingUser = users.find((user) => {
        return user.email == email;
    });

    console.log(existingUser.email);

    if (existingUser != null) {
        if (existingUser.email == email) {
            if (existingUser.password == password) return res.status(200).json({
                status: 'success',
                message: 'successfully logged in'
            });
            else return res.status(400).json({
                status: 'failed',
                errors: 'incorrect password'
            });
        }
    } else return res.status(400).json({
        status: 'failed',
        errors: 'user not found'
    });
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        errors: 'Path not found'
    });
});

const port = process.env.PORT || 2000;
app.listen(port, () => console.log(`App is listening on port ${port}`));
