const { FightRoom } = require('../models');

module.exports = {
    create: async(req, res) => {
        try {
            const room = await FightRoom.create({
                name: req.body.name,
                p1_id: req.cookies.id
            });
            res.redirect('/' + room.id);
        } catch (err) {
            console.log(err);
            res.status(422).json(err);
        }
    },
    join: async(req, res) => {
        const name = req.body.name;
        const id = req.cookies.id;

        const room = await FightRoom.findOne({
            where: { name: name }
        });

        if (room.p1_id == id) return res.status(422).json({ error: "Error" });

        await FightRoom.update({
            p2_id: id,
        }, { where: { id: room.id } });

        res.redirect('/' + room.id);
    },
    updateP1Choice: async(req, res) => {
        const choice = req.body.choice;
        const room = FightRoom.findOne({
            where: { id: req.params.id }
        });
        await FightRoom.update({
            p1_choice: choice,
        }, { where: { id: req.params.id } });

        res.redirect('/' + room.id);
    },
    updateP2Choice: async(req, res) => {
        const choice = req.body.choice;
        const room = FightRoom.findOne({
            where: { id: req.cookies.id }
        });
        await FightRoom.update({
            p2_choice: choice,
        }, { where: { id: req.params.id } });

        res.redirect('/' + room.id);
    },
    updateWinner: async(req, res) => {
        const room_id = req.cookies.room_id
        const winner = req.body.winner;
        const room = FightRoom.findOne({
            where: { id: room_id }
        });
        await FightRoom.update({
            winner: winner,
        }, { where: { id: room_id } });

        res.redirect('/' + room_id);
    },
    fightRoomViews: async(req, res) => {
        try {
            const p_id = req.cookies.id;
            var player;
            const room = await FightRoom.findOne({
                where: { id: req.params.id }
            });

            if (room.p1_id == p_id) player = "p1";
            else player = "p2";

            res.cookie('player', player);
            res.cookie('room_id', room.id);

            res.render('game', {
                room,
                player
            });
        } catch (err) {
            console.log(err);
            return res.status(422).json({ error: err });
        }
    },
    createRoomViews: (req, res) => {
        res.render('game/createRoom');
    },
    joinRoomViews: (req, res) => {
        res.render('game/joinRoom');
    },
    getRoomData: async(req, res) => {

        const room_id = req.cookies.room_id;

        try {
            const data = await FightRoom.findOne({
                where: {
                    id: room_id
                }
            });
            res.status(200).json({ roomData: data })
        } catch (err) {
            console.log(err);
        }
    },
}
