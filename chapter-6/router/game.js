const router = require('express').Router();
const game = require('../controllers/gameController');
var cookieParser = require('cookie-parser');
router.use(cookieParser());

router.get('/create-room', game.createRoomViews);
router.post('/api/v1/create-room', game.create);
router.get('/join-room', game.joinRoomViews);
router.post('/api/v1/join-room', game.join);
router.get('/:id', game.fightRoomViews);
router.post('/p1/:id/', game.updateP1Choice);
router.post('/p2/:id/', game.updateP2Choice);
router.get('/api/:id/', game.getRoomData);
router.post('/api/v1/winner', game.updateWinner);

module.exports = router;
