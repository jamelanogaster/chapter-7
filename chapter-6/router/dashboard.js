const router = require('express').Router();

router.get('/dashboard', (req, res) => {
    UserGame.findAll()
        .then(users => {
            res.render('challenge/index', {
                users
            });
        });
});
router.get('/login', (req, res) => {
    res.render('challenge/login');
});
router.get('/register', (req, res) => {
    res.render('challenge/register');
});
router.get('/users/:id', (req, res) => {
    var biodata;
    var history;
    UserGameHistory.findOne({
        where: { userId: req.params.id }
    }).then(user => {
        history = user;
    });
    UserGameBiodata
        .findOne({
            where: { userId: req.params.id }
        }).then(user => {
            biodata = user;
            console.log(biodata);
        });
    UserGame.findOne({
            where: { id: req.params.id }
        })
        .then(user => {
            res.render('challenge/show', {
                user,
                biodata,
                history,
            })
        });
});

router.get('/edit/:id', (req, res) => {
    var biodata;
    var history;
    var id = req.params.id;
    UserGameHistory.findOne({
        where: { userId: req.params.id }
    }).then(user => {
        history = user;
    });
    UserGameBiodata
        .findOne({
            where: { userId: req.params.id }
        }).then(user => {
            biodata = user;
        });
    UserGame.findOne({
            where: { id: req.params.id }
        })
        .then(user => {
            res.render('challenge/edit', {
                user,
                biodata,
                history,
                id
            })
        });
});
router.get('/delete/:id', (req, res) => {
    UserGameHistory.destroy({
        where: { userId: req.params.id }
    }).then(user => {
        UserGameBiodata.destroy({
            where: { userId: req.params.id }
        }).then(user => {
            UserGame.destroy({
                where: { id: req.params.id }
            }).then(user => {
                res.redirect('/');
            }).catch(err => {
                console.log(err);
                res.status(422).json("Can't delete user");
            });
        }).catch(err => {
            console.log(err);
            res.status(422).json("Can't delete user");
        });
    }).catch(err => {
        console.log(err);
        res.status(422).json("Can't delete user");
    });
});

module.exports = router;
