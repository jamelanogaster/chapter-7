const express = require('express');
const app = express();
const { UserGame } = require('./models');
const { UserGameBiodata } = require('./models');
const { UserGameHistory } = require('./models');

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '../../'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const router = require('./router');
app.use(router);

// API Endpoint
app.get('/api/v1/users', (req, res) => {
    UserGame.findAll()
        .then(users => {
            res.status(200).json(users);
        });
});

app.post('/api/v1/users', (req, res) => {
    UserGame.create({
            username: req.body.username,
            password: req.body.password,
        })
        .then(user => {
            res.redirect('/login');
        }).catch(err => {
            console.log(err);
            res.status(422).json("Can't create user");
        });
});

app.post('/api/v1/users/:id', (req, res) => {
    UserGame.update({
            username: req.body.username,
            password: req.body.password
        }, { where: { id: req.params.id } })
        .then(user => {
            UserGameBiodata.upsert({
                name: req.body.name,
                gender: req.body.gender,
                address: req.body.address,
                phone: req.body.phone,
                userId: req.params.id
            }).then(biodata => {
                UserGameHistory.upsert({
                    userId: req.params.id,
                    playTime: req.body.playTime,
                    score: req.body.score
                }).then(history => {
                    res.redirect('/');
                }).catch(err => {
                    console.log(err);
                    res.status(422).json("Can't update history");
                })
            }).catch(err => {
                console.log(err);
                res.status(422).json("Can't update biodata");
            })
        }).catch(err => {
            console.log(err);
            res.status(422).json("Can't update user");
        })
})

app.post('/api/v1/login', (req, res) => {
    UserGame.findOne({
            where: { username: req.body.username }
        })
        .then((user) => {
            if (user.password == req.body.password) res.redirect('/');
            else res.status(422).json("Wrong password");
        }).catch(err => {
            res.status(422).json("Username not found");
        })
});

app.get('/delete/:id', (req, res) => {
    UserGameHistory.destroy({
        where: { userId: req.params.id }
    }).then(user => {
        UserGameBiodata.destroy({
            where: { userId: req.params.id }
        }).then(user => {
            UserGame.destroy({
                where: { id: req.params.id }
            }).then(user => {
                res.redirect('/');
            }).catch(err => {
                console.log(err);
                res.status(422).json("Can't delete user");
            });
        }).catch(err => {
            console.log(err);
            res.status(422).json("Can't delete user");
        });
    }).catch(err => {
        console.log(err);
        res.status(422).json("Can't delete user");
    });
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: 'failed',
        errors: err.message
    });
});

app.use((req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    res.status(404).json({
        status: 'failed',
        errors: 'Path not found'
    });
});
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`App is listening on port ${port}`));
