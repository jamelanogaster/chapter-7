'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FightRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  FightRoom.init({
    name: DataTypes.STRING,
    p1_id: DataTypes.INTEGER,
    p2_id: DataTypes.INTEGER,
    p1_choice: DataTypes.INTEGER,
    p2_choice: DataTypes.INTEGER,
    winner: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'FightRoom',
  });
  return FightRoom;
};