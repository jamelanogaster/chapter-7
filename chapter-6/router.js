const express = require('express');
const router = express.Router();

const dashboardRouter = require('./router/dashboard');
const gameRouter = require('./router/game');

var cookieParser = require('cookie-parser');
router.use(cookieParser());

router.use(dashboardRouter);
router.use(gameRouter);

router.get('/', (req, res) => {
    res.render('index');
});

module.exports = router;
