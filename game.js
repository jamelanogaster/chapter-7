var rps = document.getElementsByClassName('icon');
var grid = document.getElementsByClassName('col-sm');
var result = document.getElementById('result');
var p1 = document.getElementsByClassName('p1');
var p2 = document.getElementsByClassName('p2');

class Player {
    getRandomChoice() {
        var choice = Math.floor(Math.random() * 3) + 1;
        return choice;
    }
}

class Game {
    constructor(hasResult) {
        this.hasResult = hasResult;
    }

    isHasResult() {
        return this.hasResult;
    }

    async getWinner(player, com) {
        if (com == 1) {
            document.getElementById('enemy-batu').classList.add('selected');
            document.getElementById('enemy-kertas').classList.remove('selected');
            document.getElementById('enemy-gunting').classList.remove('selected');
        } else if (com == 2) {
            document.getElementById('enemy-kertas').classList.add('selected');
            document.getElementById('enemy-batu').classList.remove('selected');
            document.getElementById('enemy-gunting').classList.remove('selected');
        } else {
            document.getElementById('enemy-gunting').classList.add('selected');
            document.getElementById('enemy-kertas').classList.remove('selected');
            document.getElementById('enemy-batu').classList.remove('selected');
        }

        if (player == 'rock') {
            document.getElementById('player-batu').classList.add('selected');
            document.getElementById('player-kertas').classList.remove('selected');
            document.getElementById('player-gunting').classList.remove('selected');
        } else if (player == 'paper') {
            document.getElementById('player-kertas').classList.add('selected');
            document.getElementById('player-batu').classList.remove('selected');
            document.getElementById('player-gunting').classList.remove('selected');
        } else {
            document.getElementById('player-gunting').classList.add('selected');
            document.getElementById('player-kertas').classList.remove('selected');
            document.getElementById('player-batu').classList.remove('selected');
        }

        if (player == 'rock' && com == 1) {
            result.innerHTML = 'DRAW';
        } else if (player == 'rock' && com == 2) {
            result.innerHTML = 'PLAYER 2 WINS';
        } else if (player == 'rock' && com == 3) {
            result.innerHTML = 'PLAYER 1 WINS';
        } else if (player == 'paper' && com == 1) {
            result.innerHTML = 'PLAYER 1 WINS';
        } else if (player == 'paper' && com == 2) {
            result.innerHTML = 'DRAW';
        } else if (player == 'paper' && com == 3) {
            result.innerHTML = 'PLAYER 2 WINS';
        } else if (player == 'scissors' && com == 1) {
            result.innerHTML = 'PLAYER 2 WINS';
        } else if (player == 'scissors' && com == 2) {
            result.innerHTML = 'PLAYER 1 WINS';
        } else {
            result.innerHTML = 'DRAW';
        }

        const data = await getRoomData();
        if (result.innerHTML == 'PLAYER 1 WINS') {
            console.log('p1 wins');
            await fetch(`/api/v1/winner`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    winner: data.roomData.p1_id
                })
            });
        } else {
            console.log('p2 wins');
            await fetch(`/api/v1/winner`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    winner: data.roomData.p2_id
                })
            });
        }

        if (result.innerHTML == 'DRAW') {
            result.classList.remove('win-text');
            result.classList.remove('vs-text');
            result.className += ' text-style draw-text';
        } else {
            result.classList.remove('draw-text');
            result.classList.remove('vs-text');
            result.className += ' text-style win-text';
        }

        this.hasResult = true;
    }

    refreshGame() {
        result.classList.remove('draw-text');
        result.classList.remove('win-text');
        result.classList.remove('text-style');
        result.classList.remove('waiting-style');
        result.className += ' vs-text';
        result.innerHTML = "VS";

        var gameIcon = document.getElementsByClassName('rps');

        for (var i = 0; i < gameIcon.length; i++) {
            gameIcon[i].classList.remove('selected');
        }

        this.hasResult = false;
    }
}

var game = new Game(false);
var player = new Player();
var playerPlaying = getCookie('player');
const roomId = getCookie('room_id');
console.log(playerPlaying);

for (var i = 0; i < grid.length; i++) {
    grid[i].className += ' d-flex justify-content-center align-items-center';
}

if (playerPlaying == 'p1') {
    for (var i = 0; i < p1.length; i++) {
        p1[i].className += ' icon';
    }
} else {
    for (var i = 0; i < p2.length; i++) {
        p2[i].className += ' icon';
    }
}


function goToHome() {
    window.location.href = '/';
}

async function rpsClicked(choice) {
    var choiceInt;
    if (playerPlaying == 'p1' && (choice == 1 || choice == 2 || choice == 3)) return;
    if (playerPlaying == 'p2' && (choice == 'rock' || choice == 'paper' || choice == 'scissors')) return;
    console.log(choice);

    if (choice == 'rock' || choice == 'paper' || choice == 'scissors') {
        if (choice == 'rock') choiceInt = 1;
        else if (choice == 'paper') choiceInt = 2;
        else choiceInt = 3;
        await fetch(`/p1/${roomId}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                choice: choiceInt
            })
        });
    } else {
        await fetch(`/p2/${roomId}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                choice: choice
            })
        });
    }

    const data = await getRoomData();
    console.log(data);

    var p1_pick = data.roomData.p1_choice;
    var p2_pick = data.roomData.p2_choice;
    var p1_pick_str;

    if (p1_pick == 1) p1_pick_str = 'rock';
    else if (p1_pick == 2) p1_pick_str = 'paper';
    else p1_pick_str = 'scissors';

    if (p1_pick == null || p2_pick == null) {
        result.innerHTML = "Waiting";
        result.classList.remove('draw-text');
        result.classList.remove('vs-text');
        result.className += ' text-style waiting-text';
        return;
    }

    console.log('result', game.isHasResult());
    if (!game.isHasResult()) game.getWinner(p1_pick_str, p2_pick);

}

function refreshGame() {
    game.refreshGame();
}

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

async function getRoomData() {
    try {
        data = await fetch(`/api/${roomId}`);
        data = await data.json();
        return data;
    } catch (error) {
        console.log(error);
        return false;
    }
}